function problem1(boards, boardData) {
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            let requiredBoard = boards.find((board) => {
                return (board['id'] === boardData || board['name'] === boardData);
            });
    
            if (requiredBoard) {
               resolve(requiredBoard);
            } else {
                reject('Data not found');
            }
    
        }, 2 * 1000);

    })
   

}

module.exports = problem1;