let boards = require('../data/boards.json');
let cards = require('../data/cards.json');
let lists = require('../data/lists.json');

let problem4 = require('../callback4.js');

let expectedResult = [
    {
        id: '1',
        description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
        id: '2',
        description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    },
    {
        id: '3',
        description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
    }
];



problem4(boards, cards, lists).then((data) => {
    if (JSON.stringify(data) === JSON.stringify(expectedResult)) {
        console.log(data);
    } else {
        console.log('Wrong output');
    }
}).catch((err) => {
    console.log(err);
})
