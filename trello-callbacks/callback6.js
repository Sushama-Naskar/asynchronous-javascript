let fs = require('fs');

let problem1 = require('./callback1.js');
let problem2 = require('./callback2.js');
let problem3 = require('./callback3.js');

async function problem6(boards, cards, lists) {
    
        setTimeout(() => {
            let boardName = "Thanos";
            let requiredList = [];
            
            problem1(boards, boardName).then((requiredBoard) => {
                console.log(requiredBoard);
                let boardId = requiredBoard['id'];
                return problem2(lists, boardId)

            }).then((data) => {
                console.log(data);
                requiredList = data;
                let listName = requiredList.map(function (list) {
                    return list['id'];
                });
                listName.map(function (listId) {
                    problem3(cards, listId).then((data) => {
                        console.log(data);
                    })
                });

            }).catch((err) => {

                console.log(err);
            })

        }, 2 * 1000);
    
}


module.exports = problem6;

