let fs = require('fs');
let path = require('path');

function appendFileNames(filePath, fileName, data) {
    return new Promise((resolve, reject) => {
        fs.appendFile(path.join(filePath, fileName), data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }

        })

    })


}
function readFile(filePath, fileName, cb) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.join(filePath, fileName), "utf-8", (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        })

    })

}
function writeFile(filePath, fileName, writeData, cb) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(filePath, fileName), writeData, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve();
            }

        })

    })


}
function removeFile(filePath, fileNames, cb) {
    return new Promise((resolve, reject) => {
        fileNames.forEach((fileName) => {
            fs.rm(path.join(filePath, fileName), (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })

        })


    })



}

function problem2(dataFilePath, filePath, cb) {


    readFile(dataFilePath, '/lipsum.txt').then((lipsumData) => {
        console.log(`Read lipsum.txt.`);
        let upperCaseData = lipsumData.toUpperCase();
        return writeFile(filePath, '/upperCaseData.txt', upperCaseData)

    }).then(() => {
        console.log(`Data converted to uppercase and added to upperCaseData.txt.`);
        return writeFile(filePath, '/filenames.txt', 'upperCaseData.txt')

    }).then(() => {
        console.log(`upperCaseData.txt name added to filenames.txt. `);
        return readFile(filePath, '/upperCaseData.txt');

    }).then((upperCaseData) => {
        console.log(`Read upperCaseData.txt.`);
        let lowerCaseData = upperCaseData.toLowerCase().split('.').join('\n');
        return writeFile(filePath, '/lowerCaseData.txt', lowerCaseData);


    }).then(() => {
        console.log(`Data converted to lowercase and contents split into sentences and stored at lowercaseData.txt.`);
        return appendFileNames(filePath, '/filenames.txt', '\nlowerCaseData.txt',)

    }).then(() => {
        console.log(`lowerCaseData.txt name added to filenames.txt. `);
        return readFile(filePath, '/lowerCaseData.txt')

    }).then((lowerCaseData) => {
        console.log('Read lowerCaseData.txt file');
        let sortedData = lowerCaseData.split('\n').sort().join('\n');
        return writeFile(filePath, '/sortedData.txt', sortedData)

    }).then(() => {
        console.log('Data sorted and stored in sortedData.txt file');
        return appendFileNames(filePath, '/filenames.txt', '\nsortedData.txt')

    }).then(() => {
        console.log(`sortedData.txt name added to filenames.txt. `);
        return readFile(filePath, '/filenames.txt');

    }).then((fileNames) => {
        console.log('Read filenames.txt file');
        let file = fileNames.split('\n');
        return removeFile(filePath, file);

    }).then(() => {
        cb(null,`Files deleted`);

    }).catch((err) => {
        cb(err);
    })
   
                                                                                             
  

}

module.exports = problem2;
