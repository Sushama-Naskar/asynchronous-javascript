let fs = require('fs');

let problem1 = require('./callback1.js');
let problem2 = require('./callback2.js');
let problem3 = require('./callback3.js');


function problem5(boards, cards, lists, cb) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            let boardName = "Thanos";
            let requiredList = [];
            let result = [];
            problem1(boards, boardName).then((requiredBoard) => {
                console.log(requiredBoard);
                let boardId = requiredBoard['id'];
                return problem2(lists, boardId)

            }).then((data) => {
                console.log(data);
                requiredList = data;
                let listId = requiredList.find(function (list) {
                    return list['name'] === 'Mind';
                })

                return problem3(cards, listId['id']);

            }).then((card) => {
                console.log(card);
                result.push(card);
                let listIdSpace = requiredList.find(function (list) {
                    return list['name'] === 'Space';
                });
                return problem3(cards, listIdSpace['id']);


            }).then((card) => {
                result.push(card);
                resolve(result);
            }).catch((err) => {
                reject(err);
            })

        }, 2 * 1000);
    })
}


module.exports = problem5;