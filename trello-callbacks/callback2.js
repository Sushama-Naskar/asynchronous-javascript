function problem2(lists,boardId){
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            if(lists[boardId]){
             resolve(lists[boardId]);
         }else{
             reject('Data not found');
         }
        }, 2 * 1000);

    })
   

}

module.exports=problem2;