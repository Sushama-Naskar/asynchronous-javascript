let fs = require('fs');
const { createBrotliCompress } = require('zlib');

let problem1 = require('./callback1.js');
let problem2 = require('./callback2.js');
let problem3 = require('./callback3.js');


function problem4(boards, cards, lists) {
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            let boardName = "Thanos";
            problem1(boards, boardName).then((requiredBoard)=>{
                console.log(requiredBoard);
                let boardId = requiredBoard['id'];
                return problem2(lists, boardId)
                
            }).then((requiredList)=>{
                console.log(requiredList);
                let listId = requiredList.find(function (list) {
                return list['name'] === 'Mind';
                })
                return problem3(cards, listId['id']);

            }).then((data)=>{
                resolve(data);
            }).catch((err)=>{
                reject(err);
            })
            
        }, 2*1000);
    })
}



module.exports = problem4;