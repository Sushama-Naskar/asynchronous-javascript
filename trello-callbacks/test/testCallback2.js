let fs = require('fs');

let lists = require('../data/lists.json');
let problem2 = require('../callback2.js');

let boardId = "mcu453ed";
let expectedResult = [
    { id: 'qwsa221', name: 'Mind' },
    { id: 'jwkh245', name: 'Space' },
    { id: 'azxs123', name: 'Soul' },
    { id: 'cffv432', name: 'Time' },
    { id: 'ghnb768', name: 'Power' },
    { id: 'isks839', name: 'Reality' }
];

problem2(lists, boardId).then((data) => {
    if (JSON.stringify(expectedResult) === JSON.stringify(data)) {
        console.log(data);
    } else {
        console.log('Wrong Output');
    }


}).catch((err) => {
    console.log(err);
})

