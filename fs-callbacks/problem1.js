let fs = require('fs');

function  createFolder(dirName){
    return new Promise((resolve,reject)=>{
        fs.mkdir(dirName, (err) => {
            if (err) {
               reject(err);
            } else {
                resolve('Folder create');
            }
        })

    })
}

function createFile(randomJsonFilePath){
      let filePath = randomJsonFilePath;
      return new Promise((resolve,reject)=>{
        filePath.forEach(file => {
            let data = "";
            fs.writeFile(file, data, (err) => {
                if (err) {
                  reject(err);
                } else {
                    resolve('File created');
    
                }
    
            })
        })

      })
  

}

function deleteFiles(dirName){
    return new Promise((resolve,reject)=>{
        fs.rmdir(dirName,{recursive:true},(err)=>{
            if (err) {
              reject(err);
            } else {
                resolve('Folder deleted');
            }
        });

    })



}

function problem1(dirName,randomJsonFilePath){
    return new Promise((resolve,reject)=>{
        createFolder(dirName).then((str)=>{
            console.log(str);
            return createFile(randomJsonFilePath);
         
         }).then((str)=>{
             console.log(str);
             return deleteFiles(dirName);
         
         }).then((str)=>{
             
             resolve(str);
         
         }).catch((err)=>{
             reject(err);
         
         });

    })
    

}


module.exports=problem1;