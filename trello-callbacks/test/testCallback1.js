let fs = require('fs');

let boards = require('../data/boards.json');
let problem1 = require('../callback1.js');

let boardId = "mcu453ed";
let expectedResult = { id: 'mcu453ed', name: 'Thanos', permissions: {} };


problem1(boards, boardId).then((data) => {
  if (JSON.stringify(expectedResult) === JSON.stringify(data)) {
    console.log(data);
  } else {
    console.log('Wrong Output');
  }


}).catch((err) => {
  console.log(err);
})



