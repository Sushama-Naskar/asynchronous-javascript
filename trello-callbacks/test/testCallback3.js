let fs = require('fs');

let cards = require('../data/cards.json');
let problem3 = require('../callback2.js');

let listId = "qwsa221";
let expectedResult = [
  {
    id: '1',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  },
  {
    id: '2',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  },
  {
    id: '3',
    description: 'Having acquired the Power Stone, one of the six Infinity Stones,from the planet Xandar'
  }
];

problem3(cards, listId).then((data) => {
  if (JSON.stringify(expectedResult) === JSON.stringify(data)) {
    console.log(data);
  } else {
    console.log('Wrong Output');
  }


}).catch((err) => {
  console.log(err);
})
